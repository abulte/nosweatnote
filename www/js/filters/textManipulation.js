angular.module('nsn.filters')

.filter('firstLine', function () {
  return function (input) {
    if (!input) return
    return input.split('\n')[0]
  }
})

.filter('toBrs', function ($sce) {
  return function (input) {
    if (!input) return
    return $sce.trustAsHtml(input.split('\n').join('<br>'))
  }
})
