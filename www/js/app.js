// Ionic Starter App

angular.module('nsn', [
  'ionic',
  'nsn.components',
  'nsn.services',
  'nsn.filters',
  'ngStorage'
])

.run(function($ionicPlatform, $templateCache) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true)

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true)
    }
    if(window.StatusBar) {
      StatusBar.styleDefault()
    }
  })

  $templateCache.put('notesListComponent.html', '<notes-list></notes-list')
})

.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider

  .state('login', {
    url: '/login',
    templateUrl: 'js/components/login/login.html',
    controller: 'LoginCtrl',
    controllerAs: '$ctrl'
  })

  .state('notes-list', {
    url: '/list',
    templateUrl: 'js/components/notesList/notesList.html',
    controller: 'NotesListCtrl',
    controllerAs: '$ctrl'
  })

  .state('notes-add', {
    url: '/add',
    templateUrl: 'js/components/notesAdd/notesAdd.html',
    controller: 'NotesAddCtrl',
    controllerAs: '$ctrl'
  })

  .state('note-details', {
    url: '/notes/:noteId',
    templateUrl: 'js/components/noteDetails/noteDetails.html',
    controller: 'NoteDetailsCtrl',
    controllerAs: '$ctrl'
  })

  // .state('app', {
  //   url: '/app',
  //   abstract: true,
  //   templateUrl: 'templates/menu.html',
  //   controller: 'MainCtrl'
  // })

  $urlRouterProvider.otherwise('/list')

  // http 401 interceptor
  $httpProvider.interceptors.push('authHttpResponseInterceptor')
})

.factory('authHttpResponseInterceptor', function($q, $location){
  return {
    response: function(response){
      if (response.status === 401) {
        console.log("Response 401")
      }
      return response || $q.when(response)
    },
    responseError: function(rejection) {
      if (rejection.status === 401) {
        console.log("Response Error 401",rejection)
        $location.path('/login').search('returnTo', $location.path())
      }
      return $q.reject(rejection)
    }
  }
})
