
function LoginCtrl ($state, User) {
  var $ctrl = this
  $ctrl.user = {}

  $ctrl.login = function () {
    User.login($ctrl.user.username, $ctrl.user.password).then(function (token) {
      $state.go('notes-list')
    }, function (err) {
      console.log('Login error', err)
    })
  }
}

angular.module('nsn.components').controller('LoginCtrl', LoginCtrl)
