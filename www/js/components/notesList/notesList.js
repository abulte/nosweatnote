
function NotesListCtrl ($ionicLoading, NotesAPI) {
  var $ctrl = this

  $ctrl.refresh = function () {
    $ionicLoading.show()
    $ctrl.notes = []
    NotesAPI.query(function (notes) {
      if (notes.data) {
        notes.data.forEach(function (note) {
          $ctrl.notes.push(NotesAPI.get({noteId: note.key}))
        })
        $ionicLoading.hide()
      }
    })
  }

  $ctrl.refresh()
}

angular.module('nsn.components').controller('NotesListCtrl', NotesListCtrl)
