
function NotesAddCtrl ($state, NotesAPI) {
  var $ctrl = this
  $ctrl.note = {}

  $ctrl.add = function () {
    NotesAPI.add($ctrl.note, function () {
      $state.go('notes-list')
    }, function (error) {
      console.log('error', error)
    })
  }

}

angular.module('nsn.components').controller('NotesAddCtrl', NotesAddCtrl)
