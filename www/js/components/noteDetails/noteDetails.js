
function NoteDetailsCtrl ($scope, $state, $stateParams, NotesAPI) {
  var $ctrl = this
  $ctrl.note = NotesAPI.get($stateParams)
  $ctrl.mode = 'VIEW'

  console.log($ctrl.note)

  $ctrl.edit = function () {
    $ctrl.mode = 'EDIT'
  }

  $ctrl.save = function () {
    NotesAPI.save($stateParams, $ctrl.note, function (note) {
      console.log('saved', note)
    }, function (error) {
      console.log('error', error)
    })
    $ctrl.mode = 'VIEW'
  }

  $ctrl.delete = function () {
    NotesAPI.save($stateParams, {deleted: 1}, function (note) {
      NotesAPI.delete($stateParams, function () {
        $state.go('notes-list')
      })
    }, function (error) {
      console.log('error', error)
    })
  }

  // save when leaving
  $scope.$on('$destroy', function () {
    if ($ctrl.mode === 'EDIT') {
      $ctrl.save()
    }
  })

}

angular.module('nsn.components').controller('NoteDetailsCtrl', NoteDetailsCtrl)
