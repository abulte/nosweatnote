angular.module('nsn.services')

.factory('NotesAPI', function ($resource, User, API_BASE_URL) {
  var url_params = User.getCurrentUser()
  return $resource(API_BASE_URL + '/api2/data', url_params, {
    query: {
      method: 'GET',
      url: API_BASE_URL + '/api2/index',
    },
    get: {
      method: 'GET',
      url: API_BASE_URL + '/api2/data/:noteId',
      isArray: false
    },
    add: {
      method: 'POST',
      url: API_BASE_URL + '/api2/data',
      isArray: false
    },
    save: {
      method: 'POST',
      url: API_BASE_URL + '/api2/data/:noteId',
      isArray: false
    },
    delete: {
      method: 'DELETE',
      url: API_BASE_URL + '/api2/data/:noteId',
      isArray: false
    }
  })
})

.factory('UserAPI', function ($resource, API_BASE_URL) {
  return $resource(API_BASE_URL + '/api/login', {}, {
    login: {
      method: 'POST',
      isArray: false,
      transformResponse: function (data) {
        // transform a raw text data to json
        return {
          token: data
        }
      }
    }
  })
})
