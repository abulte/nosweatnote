angular.module('nsn.services')

.factory('User', function ($q, $localStorage, UserAPI) {
  $localStorage.currentUser = $localStorage.currentUser || {}

  var setUser =function (username, token) {
    $localStorage.currentUser = {
      auth: token,
      email: username
    }
  }

  var getCurrentUser =function (token) {
    return $localStorage.currentUser
  }

  var hasCurrentUser =function (token) {
    return !!$localStorage.currentUser.token
  }

  return {
    login: function (username, password) {
      var deferred = $q.defer()
      var encodedString = btoa('email=' + username + '&password=' + password)
      UserAPI.login(encodedString, function (userInfo) {
        setUser(username, userInfo.token)
        deferred.resolve(getCurrentUser())
      }, function (err) {
        deferred.reject(err)
      })
      return deferred.promise
    },
    getCurrentUser: getCurrentUser,
    hasCurrentUser: hasCurrentUser
  }
})
